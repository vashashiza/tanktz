﻿using System;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    private Tank player;

    private void Player_OnMove()
    {
        if (player != null)
        {
            transform.position = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
        }
    }

    private void Start()
    {
        player = GameController.Instance.Player;
        player.OnMove += Player_OnMove;
    }   
}
