﻿using MySource;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviourSingleton<Pool>
{
    [SerializeField]
    private List<MonoBehaviour> iPullablePrefabs = new List<MonoBehaviour>();

    private Dictionary<UnitType, List<IPoolable>> pullableLists = new Dictionary<UnitType, List<IPoolable>>();

    public void Add(IPoolable unit)
    {
        unit.Off();

        List<IPoolable> list;

        if (!pullableLists.TryGetValue(unit.Type, out list))
        {
            list = new List<IPoolable>();
            pullableLists.Add(unit.Type, list);
        }
        list.Add(unit);
    }

    public T Get<T>(UnitType type) where T : MonoBehaviour
    {
        List<IPoolable> list;
        if (pullableLists.TryGetValue(type, out list))
        {
            if (list.Count > 0)
            {
                IPoolable obj = list[0];
                list.Remove(obj);
                obj.ResetParametrs();

                return obj as T;
            }
        }

        MonoBehaviour prefab = iPullablePrefabs.Find((obj) =>
        {
            return obj is T && obj is IPoolable && (obj as IPoolable).Type == type;
        });

        if (prefab != null)
        {
            T instanse = Instantiate(prefab as T);
            (instanse as IPoolable).ResetParametrs();
            return instanse;
        }
        else
        {
            Debug.LogWarning("prefab not found");
        }

        return null;
    }
}
