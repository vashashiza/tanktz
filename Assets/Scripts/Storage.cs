﻿using MySource;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Storage : ScriptableObjectSingleton<Storage>
{
    public int[] bonusesNumber = new int[2];
    public int[] enemiesNumber = new int[2];
}
