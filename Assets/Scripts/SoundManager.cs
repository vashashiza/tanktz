﻿using MySource;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviourSingleton<SoundManager>
{ 
    [SerializeField]
    private List<AudioSource> audioSources = new List<AudioSource>();

    private List<AudioSource> instantiatedAudioSources = new List<AudioSource>();

    private List<AudioSource> playingSoundAudioSourcesList = new List<AudioSource>();

    private float soundSpeed = 1;
    public float SoundSpeed
    {
        get
        {
            return soundSpeed;
        }

        set
        {
            soundSpeed = value;
            PlaySoundWithNewSpeed();
        }
    }

    public void PlaySoundWithNewSpeed()
    {
        foreach (AudioSource item in instantiatedAudioSources)
        {
            item.pitch = SoundSpeed;
        }
    }

    public void PlaySound(string clipName, bool loop = false)
    {
        AudioSource audioSource = playingSoundAudioSourcesList.Find(sound => sound.clip.name == clipName);

        if (audioSource != null)
        {
            instantiateSound(clipName);
            return;
        }

        audioSource = instantiatedAudioSources.Find(x => x.clip.name == clipName);

        if (audioSource == null)
        {
            audioSource = instantiateSound(clipName);
        }
        else
        {
            audioSource.Play();
            addPlayingSoundTemporarily(audioSource);
        }

        audioSource.loop = loop;
        audioSource.pitch = SoundSpeed;
    }

    private AudioSource instantiateSound(string clipName)
    {
        AudioSource audioSource = audioSources.Find(x => x.clip.name == clipName);

        if (audioSource != null)
        {
            audioSource = Instantiate(audioSource);
            audioSource.transform.SetParent(transform);
            instantiatedAudioSources.Add(audioSource);
            addPlayingSoundTemporarily(audioSource);

            return audioSource;
        }
        else
        {
            Debug.LogWarning("no audio required");
            return null;
        }
    }

    private void addPlayingSoundTemporarily(AudioSource parSoundAudioSource)
    {
        playingSoundAudioSourcesList.Add(parSoundAudioSource);
        StartCoroutine(removePlayingSoundFromList(parSoundAudioSource));
    }

    private IEnumerator removePlayingSoundFromList(AudioSource parSoundAudioSource)
    {
        yield return new WaitForSeconds(parSoundAudioSource.clip.length);
        playingSoundAudioSourcesList.Remove(parSoundAudioSource);
    }

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(transform.gameObject);
    }
}
