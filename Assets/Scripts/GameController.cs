﻿using MySource;
using UnityEngine;

public class GameController : MonoBehaviourSingleton<GameController>
{
    [SerializeField]
    private float moveRange = 50f;
    public float MoveRange
    {
        get
        {
            return moveRange;
        }

        private set
        {
            moveRange = value;
        }
    }


    [SerializeField]
    private Tank player;
    public Tank Player
    {
        get
        {
            return player;
        }

        private set
        {
            player = value;
        }
    }

    protected void Player_OnDisintegrate(DisintegratableUnit arg1, DisintegrateType arg2)
    {
        GameOver();
    }

    protected void GameOver()
    {
        Time.timeScale = 0;
    }

    protected override void Awake()
    {
        base.Awake();

        Player.OnDisintegrate += Player_OnDisintegrate;
    }
}