﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace MySource
{
    public class Source
    {

    }

    public abstract class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviourSingleton<T>
    {
        private static T instance;
        public static T Instance
        {
            get
            {
                if (!instance)
                {
                    instance = FindObjectOfType<T>();
                    if (!instance)
                    {
                        GameObject gameObject = new GameObject(typeof(T).ToString());
                        instance = gameObject.AddComponent<T>();
                    }
                }
                return instance;
            }
            protected set
            {
                if (!instance)
                {
                    instance = value;
                }
                else if (value && instance != value)
                {
                    Destroy(value);
                }
            }
        }

        /// <summary>
        /// обязательно вызвать базовай метод
        /// </summary>
        protected virtual void Awake()
        {
            Instance = this as T;
        }
    }

    [Serializable]
    public class FloatValueKeeper
    {
        [SerializeField]
        protected float max;
        public float Max
        {
            get
            {
                return max;
            }

            set
            {
                max = value;
            }
        }

        [SerializeField]
        protected float now = 0;
        public float Now
        {
            get
            {
                return now;
            }
            set
            {
                if (value <= Min)
                {
                    now = Min;
                }
                else
                {
                    now = value;
                }
            }
        }

        [SerializeField]
        protected float min = 0;
        public float Min
        {
            get
            {
                return min;
            }

            set
            {
                min = value;
            }
        }

        public FloatValueKeeper()
        {

        }

        /// <summary>
        /// min == 0
        /// </summary>
        /// <param name="maxAndNow"></param>
        public FloatValueKeeper(float maxAndNow)
        {
            max = maxAndNow;
            now = maxAndNow;
        }

        /// <summary>
        /// min == 0
        /// </summary>
        /// <param name="max"></param>
        /// <param name="now"></param>
        public FloatValueKeeper(float max, float now)
        {
            this.max = max;
            this.now = now;
        }

        public FloatValueKeeper(float max, float now, float min) : this(max, now)
        {
            this.Min = min;
        }

        public static implicit operator float(FloatValueKeeper floatValueKeeper)
        {
            return floatValueKeeper.Now;
        }

        public static FloatValueKeeper operator +(FloatValueKeeper intValueKeeper, float x)
        {
            intValueKeeper.Now += x;
            return intValueKeeper;
        }

        public static FloatValueKeeper operator -(FloatValueKeeper intValueKeeper, float x)
        {
            intValueKeeper.Now -= x;
            return intValueKeeper;
        }
    }

    public abstract class ScriptableObjectSingleton<T> : ScriptableObject where T : ScriptableObjectSingleton<T>
    {
        private static T instance;
        public static T Instance
        {
            get
            {
                if (!instance)
                {
                    string name = typeof(T).ToString();

                    instance = Resources.Load<T>(name);

                    if (!instance)
                    {
                        instance = CreateAsset(name);
                    }
                }

                return instance;
            }
        }

        public static T CreateAsset(string name)
        {
            T scriptableObject = CreateInstance<T>();
#if UNITY_EDITOR
            string filePath = "Assets/Resources/";

            if (!Directory.Exists(filePath)) Directory.CreateDirectory(filePath);

            UnityEditor.AssetDatabase.CreateAsset(scriptableObject, filePath + name + ".asset");
            UnityEditor.AssetDatabase.SaveAssets();
#endif
            return scriptableObject;
        }
    }
}



public enum UnitType
{
    Bullet = 0,
    ExplodingBullet = 1,
    HealthPack = 11,
    ImmortalBonus = 12,
    ReloadBonus = 13,
    MoveSpeedBonus = 14,
    TurnSpeedBonus = 15,
    Knight = 101,
    Skeleton = 102,
    ZombieZero = 103,
    ZombieOne = 104,
    ZombieTwo = 105,
    Tank = 200
}

public enum DisintegrateType
{
    Collision = 0,
    SelfDestruction = 1
}