﻿using MySource;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weaponry : MonoBehaviour
{
    protected bool isReload = true;
    public bool IsReload
    {
        get
        {
            return isReload;
        }

        set
        {
            isReload = value;
            if (!isReload)
            {
                StartCoroutine(Reload());
            }
        }
    }

    [SerializeField]
    protected FloatValueKeeper reloadTime = new FloatValueKeeper();
    public FloatValueKeeper ReloadTime
    {
        get
        {
            return reloadTime;
        }

        set
        {
            reloadTime = value;
        }
    }

    protected IEnumerator Reload()
    {
        yield return new WaitForSeconds(ReloadTime);
        IsReload = true;
    }

    public abstract void Shot();
}