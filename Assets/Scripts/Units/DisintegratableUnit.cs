﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DisintegratableUnit : MonoBehaviour, IPoolable
{
    public event Action<DisintegratableUnit, DisintegrateType> OnDisintegrate;

    [SerializeField]
    protected UnitType type;
    public UnitType Type
    {
        get
        {
            return type;
        }
    }

    public virtual void Disintegrate(DisintegrateType type)
    {
        if ((object)this != null)
        {
            Pool.Instance.Add(this);
            OnDisintegrate?.Invoke(this, type);
        }
    }

    public virtual void Off()
    {
        gameObject.SetActive(false);
    }

    public virtual void ResetParametrs()
    {
        
    }
}

