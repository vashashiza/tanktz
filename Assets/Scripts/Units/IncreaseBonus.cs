﻿using UnityEngine;

public class IncreaseBonus : Bonus
{
    [SerializeField]
    protected float bonusValue;
    public float BonusValue
    {
        get
        {
            return bonusValue;
        }
    }
}
