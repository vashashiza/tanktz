﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class TriggerProvider : MonoBehaviour
{
    public event Action<Collider> TriggerPlayerEnter;
    public event Action<Collider> TriggerPlayerExit;

    [SerializeField]
    private Collider thisCollider;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            TriggerPlayerEnter?.Invoke(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            TriggerPlayerExit?.Invoke(other);
        }
    }
}
