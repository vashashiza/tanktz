﻿using MySource;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Weaponry
{
    [SerializeField]
    protected Transform shotPoint = null;
    [SerializeField]
    protected UnitType bulletType;

    public override void Shot()
    {
        if (IsReload)
        {
            Bullet bullet = Pool.Instance.Get<Bullet>(bulletType);
            bullet.Shot(shotPoint.position, shotPoint.rotation);
            SoundManager.Instance.PlaySound("SE_Fire");
            IsReload = false;
        }
    }

    protected void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shot();
        }
    }
}
