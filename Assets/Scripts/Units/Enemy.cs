﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : TakingDamageUnit, ISetDamage
{  
    [SerializeField]
    protected UnityEngine.AI.NavMeshAgent Agent;

    [SerializeField]
    protected Animator animator;

    [SerializeField]
    protected Collider bodyCollider;
    [SerializeField]
    protected TriggerProvider observer;

    [SerializeField]
    protected float deathDelay;

    [SerializeField]
    protected float hitDelay;
    [SerializeField]
    protected float reloadTime;

    protected bool inAttack = false;

    protected Tank target;

    [SerializeField]
    protected float damage;
    public float Damage
    {
        get { return damage; }
    }

    public bool MoveActive
    {
        get
        {
            return moveActive;
        }
        set
        {
            moveActive = value;
            Agent.isStopped = !value;

            animator.SetBool("Move", value);
        }
    }

    public override void ResetParametrs()
    {
        bodyCollider.enabled = true;
        observer.gameObject.SetActive(true);
        animator.SetBool("Dead", false);
        health.Now = health.Max;
        moveSpeed.Now = moveSpeed.Min;
    }

    public override void Disintegrate(DisintegrateType type)
    {
        StartCoroutine(DelayedDeath());        
    }

    protected override void Move()
    {
        if (target)
        {
            Agent.SetDestination(target.transform.position);
        }
    }

    private IEnumerator DelayedDeath()
    {
        MoveActive = false;
        bodyCollider.enabled = false;
        observer.gameObject.SetActive(false);

        if (deathDelay > 0)
        {
            animator.SetBool("Dead", true);
            yield return new WaitForSeconds(deathDelay);
            animator.SetBool("Dead", false);
        }

        base.Disintegrate(DisintegrateType.Collision);
    }

    protected virtual IEnumerator Attack()
    {
        MoveActive = false;
        inAttack = true;

        while (target!= null && inAttack)
        {
            animator.SetBool("Attack", true);
            yield return new WaitForSeconds(hitDelay);
            animator.SetBool("Attack", false);

            if (target != null && inAttack)
            {
                target.SetDamage(Damage, type);
            }

            yield return new WaitForSeconds(reloadTime - hitDelay);
        }
        inAttack = false;
        MoveActive = true;
    }

    protected void Observer_TriggerPlayerExit(Collider obj)
    {
        if (obj.tag == "Player")
        {
            inAttack = false;
        }        
    }

    protected void Observer_TriggerPlayerEnter(Collider obj)
    {
        if (obj.tag == "Player" && !inAttack)
        {
            StartCoroutine(Attack());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            Bullet bullet = other.GetComponent<Bullet>();
            SetDamage(bullet.Damage, bullet.Type);
        }
    }


    protected void OnEnable()
    {
        Agent.speed = moveSpeed;
        MoveActive = true;
    }

    protected void Awake()
    {
        target = GameController.Instance.Player;

        Agent.speed = moveSpeed;
        MoveActive = true;

        observer.TriggerPlayerEnter += Observer_TriggerPlayerEnter;
        observer.TriggerPlayerExit += Observer_TriggerPlayerExit;
    }    
}
