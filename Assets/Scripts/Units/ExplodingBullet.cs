﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingBullet : Bullet
{
    [SerializeField]
    protected float radius;
    [SerializeField]
    protected float deathDelay = 2;

    [SerializeField]
    new private ParticleSystem particleSystem = null;

    public override void Disintegrate(DisintegrateType type)
    {
        moveActive = false;

        StartCoroutine(death(type));
    }

    protected IEnumerator death(DisintegrateType type)
    {
        yield return new WaitForSeconds(deathDelay);
        base.Disintegrate(type);
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy" || other.tag == "Obstacle")
        {
            particleSystem.Play();
            SoundManager.Instance.PlaySound("ShellExplosion");
            Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
            foreach (Collider item in colliders)
            {
                if (item.tag == "Enemy")
                {
                    Enemy enemy = item.GetComponent<Enemy>();
                    enemy.SetDamage(Damage, UnitType.Bullet);
                }
            }

            Disintegrate(DisintegrateType.Collision);
        }
    }
}
