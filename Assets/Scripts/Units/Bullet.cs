﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MovableUnit, ISetDamage
{   
    [SerializeField]
    protected float range;

    protected Vector3 startPosition;

    [SerializeField]
    private float damage;
    public float Damage
    {
        get
        {
            return damage;
        }

        private set
        {
            damage = value;
        }
    }

    public void Shot(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;
        direction = transform.forward;
        moveActive = true;
        gameObject.SetActive(true);
    }

    protected override void Move()
    {
        base.Move();

        if (Vector3.Distance(transform.position, startPosition) >= range)
        {
            Disintegrate(DisintegrateType.SelfDestruction);
        }
    }

    public override void Off()
    {
        base.Off();
        moveActive = false;
    }

    protected void OnEnable()
    {
        startPosition = transform.position;
    }

    protected void Awake()
    {
        startPosition = transform.position;
    }
}