﻿
public interface IPoolable
{
    UnitType Type { get; }

    void Off();

    void ResetParametrs();
}
