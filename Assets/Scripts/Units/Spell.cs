﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell : Weaponry
{
    public event Action<Spell> ActivateSpell;
    public event Action<Spell> DectivateSpell;

    [SerializeField]
    protected float actionTime;

    public override void Shot()
    {
        if (IsReload)
        {            
            StartCoroutine(invisibilitySpellActive());
            IsReload = false;
        }
    }

    protected IEnumerator invisibilitySpellActive()
    {
        ActivateSpell?.Invoke(this);
        yield return new WaitForSeconds(actionTime);
        gameObject.SetActive(false);
        DectivateSpell?.Invoke(this);
    }
}
