﻿using MySource;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakingDamageUnit : MovableUnit
{
    [SerializeField]
    protected FloatValueKeeper health;
    [Header("от 0 до 1; 1 - нет урона")]
    [SerializeField]
    protected FloatValueKeeper protection;

    public virtual void SetDamage(float damage, UnitType type)
    {
        health -= damage * (1 - protection);

        if (health <= 0)
        {
            Disintegrate(DisintegrateType.Collision);
        }
    }
}
