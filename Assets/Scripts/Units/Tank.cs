﻿using MySource;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : TakingDamageUnit
{
    public event Action OnMove;

    [SerializeField]
    protected FloatValueKeeper turnSpeed = new FloatValueKeeper();
    [SerializeField]
    protected List<Weapon> weapons = new List<Weapon>();
    
    [SerializeField]
    protected Spell spell;

    protected bool moveBack = false;
    protected Vector3 zeroPos = new Vector3();

    protected Weapon currentWeapon;
    private int weaponIndex = 0;

    protected Dictionary<UnitType, Coroutine> bonusEffects = new Dictionary<UnitType, Coroutine>();

    protected bool immortal;

    protected float moveRange;

    protected Coroutine hillRoutine;
    

    protected void UseBonus(Bonus bonus)
    {
        if ((object)this != null)
        {
            Coroutine effectCoroutine;
            if (bonusEffects.TryGetValue(bonus.Type, out effectCoroutine))
            {
                bonusEffects.Remove(bonus.Type);
                StopCoroutine(effectCoroutine);
            }
            switch (bonus.Type)
            {
                case UnitType.HealthPack:
                    health += (bonus as IncreaseBonus).BonusValue;
                    break;
                case UnitType.ImmortalBonus:
                    immortal = true;
                    effectCoroutine = StartCoroutine(differedAction(bonus.EffectTime, delegate ()
                    {
                        immortal = false;
                    }));
                    break;
                case UnitType.ReloadBonus:
                    Weapon weapon = currentWeapon;
                    weapon.ReloadTime.Now = weapon.ReloadTime.Min;
                    effectCoroutine = StartCoroutine(differedAction(bonus.EffectTime, delegate ()
                    {
                        weapon.ReloadTime.Now = weapon.ReloadTime.Max;
                    }));
                    break;
                case UnitType.MoveSpeedBonus:
                    moveSpeed.Now = moveSpeed.Max;
                    effectCoroutine = StartCoroutine(differedAction(bonus.EffectTime, delegate ()
                    {
                        moveSpeed.Now = moveSpeed.Min;
                    }));
                    break;
                case UnitType.TurnSpeedBonus:
                    turnSpeed.Now = turnSpeed.Max;
                    effectCoroutine = StartCoroutine(differedAction(bonus.EffectTime, delegate ()
                    {
                        turnSpeed.Now = turnSpeed.Min;
                    }));
                    break;
                default:
                    break;
            }

            bonusEffects.Add(bonus.Type, effectCoroutine);
        }
    }

    protected IEnumerator differedAction(float delay, Action call)
    {
        yield return new WaitForSeconds(delay);
        call();
    }

    public override void SetDamage(float damage, UnitType type)
    {
        if (immortal)
        {
            return;
        }

        base.SetDamage(damage, type);
    }

    protected override void Move()
    {
        Vector3 targetPos = transform.position + direction * (Time.deltaTime * moveSpeed);
        if (Vector3.Distance(zeroPos, targetPos) < moveRange)
        {
            transform.position = targetPos;
            OnMove?.Invoke();
        }
    }

    protected void ChangeWeapon(int direction)
    {
        direction = direction >= 0 ? 1 : -1;

        weapons[weaponIndex].gameObject.SetActive(false);

        weaponIndex += direction;

        if (weaponIndex >= weapons.Count) weaponIndex = 0;
        else if (weaponIndex < 0) weaponIndex = weapons.Count - 1;

        weapons[weaponIndex].gameObject.SetActive(true);

        currentWeapon = weapons[weaponIndex];
    }

    protected void CheckMoveButtonInput()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            direction = transform.forward;
            moveActive = true;
            moveBack = false;
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            direction = -transform.forward;
            moveActive = true;
            moveBack = true;
        }

        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
        {
            moveActive = false;
            moveBack = false;
        }
    }

    protected void CheckTurnButtonInput()
    {
        if (Input.GetKey(KeyCode.D))
        {
            float turnDirection = (Time.deltaTime * turnSpeed);
            if (moveBack) turnDirection = -turnDirection;

            transform.eulerAngles += new Vector3(0, turnDirection, 0);
        }

        if (Input.GetKey(KeyCode.A))
        {
            float turnDirection = -(Time.deltaTime * turnSpeed);
            if (moveBack) turnDirection = -turnDirection;

            transform.eulerAngles += new Vector3(0, turnDirection, 0);
        }
    }

    protected void CheckChangeWeaponButtonInput()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            ChangeWeapon(1);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            ChangeWeapon(-1);
        }
    }

    protected void CheckSpellButtonInput()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            spell.gameObject.SetActive(true);
            spell.Shot();
        }
    }

    protected void Spell_DectivateSpell(Spell obj)
    {
        StopCoroutine(hillRoutine);
    }

    protected void Spell_ActivateSpell(Spell obj)
    {
        hillRoutine = StartCoroutine(hill());
    }

    protected IEnumerator hill()
    {
        while (true)
        {
            health += 5;
            yield return new WaitForSeconds(1f);
        }
    }

    protected void Update()
    {
        CheckMoveButtonInput();

        CheckTurnButtonInput();

        CheckChangeWeaponButtonInput();

        CheckSpellButtonInput();
    }

    private void Start()
    {
        zeroPos = transform.position;
        currentWeapon = weapons[weaponIndex];
        moveRange = GameController.Instance.MoveRange;

        spell.ActivateSpell += Spell_ActivateSpell;
        spell.DectivateSpell += Spell_DectivateSpell;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bonus")
        {
            Bonus bonus = other.gameObject.GetComponent<Bonus>();
            UseBonus(bonus);

            bonus.Disintegrate(DisintegrateType.Collision);
        }
    }
}
