﻿using MySource;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovableUnit : DisintegratableUnit
{
    [SerializeField]
    protected FloatValueKeeper moveSpeed;

    protected bool moveActive = false;
    protected Vector3 direction = new Vector3();

    protected virtual void Move()
    {
        transform.position += direction * (Time.deltaTime * moveSpeed);
    }

    protected void FixedUpdate()
    {
        if (moveActive)
        {
            Move();
        }
    }
}