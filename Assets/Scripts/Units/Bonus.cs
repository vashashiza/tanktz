﻿using MySource;
using System;
using System.Collections;
using UnityEngine;

public class Bonus : DisintegratableUnit, IPoolable
{
    [SerializeField]
    private float lifeTime;

    [SerializeField]
    private float effectTime;
    public float EffectTime
    {
        get
        {
            return effectTime;
        }
    }

    protected IEnumerator OffBonus()
    {
        yield return new WaitForSeconds(lifeTime);
        Disintegrate(DisintegrateType.SelfDestruction);
    }

    private void Awake()
    {
        StartCoroutine(OffBonus());
    }

    private void OnEnable()
    {
        StartCoroutine(OffBonus());
    }
}
