﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{   
    [SerializeField]
    protected List<Transform> enemiesSpawnPoints = new List<Transform>();

    [SerializeField]
    protected int bonusesOnScene;
    [SerializeField]
    protected int enemiesOnScene = 10;

    private int currentEnemyCount = 0;
    private int currentBonusCount;

    protected float moveRange;

    private void SpawnEnemies()
    {
        while (currentEnemyCount != enemiesOnScene)
        {
            int spawnNumber = UnityEngine.Random.Range(0, enemiesSpawnPoints.Count);
            int prefabNumber = UnityEngine.Random.Range(Storage.Instance.enemiesNumber[0], Storage.Instance.enemiesNumber[1]+1);
            Enemy enemy = Pool.Instance.Get<Enemy>((UnitType)prefabNumber);
            
            enemy.transform.position = enemiesSpawnPoints[spawnNumber].position;
            enemy.transform.SetParent(transform);
            enemy.OnDisintegrate += Enemy_OnDisintegrate;
            enemy.gameObject.SetActive(true);

            currentEnemyCount++;
        }
    }

    private void Enemy_OnDisintegrate(DisintegratableUnit enemy, DisintegrateType arg2)
    {
        enemy.OnDisintegrate -= Enemy_OnDisintegrate;
        currentEnemyCount--;
        SpawnEnemies();
    }

    private void SpawnBonus()
    {
        float side = moveRange / Mathf.Sqrt(2f);

        while (currentBonusCount != bonusesOnScene)
        {
            Vector3 spawnPoint = new Vector3(UnityEngine.Random.Range(-side, side), 1, UnityEngine.Random.Range(-side, side));

            if (!Physics.CheckSphere(spawnPoint, 0.5f, 9))
            {
                int i = UnityEngine.Random.Range(Storage.Instance.bonusesNumber[0], Storage.Instance.bonusesNumber[1] + 1);
                Bonus bonus = Pool.Instance.Get<Bonus>((UnitType)i);
                bonus.transform.position = spawnPoint;
                bonus.transform.SetParent(transform);
                bonus.OnDisintegrate += Bonus_OnDisintegrate;
                bonus.gameObject.SetActive(true);

                currentBonusCount++;
            }
        }        
    }

    private void Bonus_OnDisintegrate(DisintegratableUnit bonus, DisintegrateType arg2)
    {
        bonus.OnDisintegrate -= Bonus_OnDisintegrate;
        currentBonusCount--;
        SpawnBonus();
    }

    private void Start()
    {
        moveRange = GameController.Instance.MoveRange;
        SpawnBonus();
        SpawnEnemies();
    }
}